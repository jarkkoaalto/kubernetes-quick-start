# Kubernetes Quick Start

OVERVIEW

This course will serve as an introduction to Kubernetes and will cover the basic installation and configuration 
needed to get a Kubernetes cluster up and running. We will also discuss deployments and pod versioning. 
In the end there will be. 

# 1. Basics and Installation

## 1.1 Getting Started

This lesson covers how to install Kubernetes on CentOS 7. Below, you will find a list of the commands used in this lesson.

##### 1.) The first thing that we are going to do is use SSH to log in to all machines. Once we have logged in, we need to elevate privileges using sudo.

sudo su

##### 2.) Disable SELinux.

setenforce 0

sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g'

/etc/sysconfig/selinux

##### 3.) Enable the br_netfilter module for cluster communication.

modprobe br_netfilter

echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables

##### 4.) Disable swap to prevent memory allocation issues.

swapoff -a

vim /etc/fstab.  ->  Comment out the swap line

##### 5.) Install Docker CE.
##### 6.) Install the Docker prerequisites.

yum install -y yum-utils device-mapper-persistent-data lvm2

##### 7.) Add the Docker repo and install Docker.
 
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce

##### 8.) Add the Kubernetes repo.
 
cat <<EOF > /etc/yum.repos.d/kubernetes.repo

[kubernetes]

name=Kubernetes

baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64

enabled=1

gpgcheck=0

repo_gpgcheck=0

gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg

        https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg

EOF

##### 9.)  Install Kubernetes.
 
yum install -y kubelet kubeadm kubectl

##### Kublet is the Kubernetes service deamon. 

##### kubectl is the management interface for the cluster. The admin can use it to interface with the API ot create and inspect objects

##### kubeadm is used to bootsrap the cluster

##### 10.) Reboot.

##### 11.) Enable and start Docker and Kubernetes.

systemctl enable docker

systemctl enable kubelet

systemctl start docker

systemctl start kubelet

##### 12.) Check the group Docker is running in.

docker info | grep -i cgroup

##### 13.) Set Kubernetes to run in the same group.

sed -i 's/cgroup-driver=systemd/cgroup-driver=cgroupfs/g' /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

##### 14.) Reload systemd for the changes to take effect, and then restart Kubernetes.


systemctl daemon-reload

systemctl restart kubelet

*Note: Complete the following section on the MASTER ONLY!

##### 15.) Initialize the cluster using the IP range for Flannel.

kubeadm init --pod-network-cidr=10.244.0.0/16

##### 16.) Copy the kubeadmin join command.

##### 17.)Deploy Flannel.

kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

##### 18.) Exit sudo and run the following:

mkdir -p $HOME/.kube

sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

sudo chown $(id -u):$(id -g) $HOME/.kube/config

##### 19.) Check the cluster state.

kubectl get pods --all-namespaces


kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

kubectl get pods --all-namespaces

 NAMESPACE     NAME                                             READY   STATUS    RESTARTS   AGE

 kube-system   coredns-86c58d9df4-4cll4                         1/1     Running   0          8m17s


kubectl get nodes

NAME                     STATUS   ROLES    AGE     VERSION

xxxxxxxx.mylabserver.com   Ready    master   8m50s   v1.13.3



Note: Complete the following steps on the NODES ONLY!

##### 20.) Run the join command that you copied earlier, then check your nodes from the master.

kubectl get nodes


###### Master 

API server: is the front end for the Kubernates control plane. All API calls are sent to this server, and the server sends commands tothe other servieces

Scheduler: when a new pod is created, the scheduler determines which node the pod will be run on. The decision is based on many factors, inculding hardware, workloads, affinity, etc.

Control Manager: Operates the cluster controllers:

	- Node controller: Responsible for noticing and responding when nodes go down.

	- Replication controller: Responsible for maintaining the correct number of pods for every replication controller object in the system

	- Endpoints Contoroller. Populates the Enpoint objects (i.e. joins services and pods)

	- Service account and token contollers: creates default accounts and API access tokens for new namespaces


###### Node

Proxy: this runs on the nodes and provides network connectivity for services on the nodes that connect to the pods. Services must be defibed via the API in order to conifigure the proxy. 

Kubelet: This is the primary node agent that runs on each node. It user PodSpec, a provided object that describes a pod, to monitor the pods on its node. The kubelet checks the state of its pods and ensures that they match the spec.

Container Runtime: This is te container manager. It can be any container runtime that is conpliant with the Open container intiative (such a docker). When Kubernites needs to instantiate a container inside of a pod, it interfaces with the container runtime to build the correct type of container.

kubectl get nodes

NAME                     STATUS   ROLES    AGE   VERSION

xxxxxxx.mylabserver.com   Ready    master   31m   v1.13.3

xxxxxxx.mylabserver.com   Ready    <none>   18m   v1.13.3

xxxxxxx.mylabserver.com   Ready    <none>   18m   v1.13.3

kubectl get pods --all-namespaces -o wide

NAMESPACE     NAME                                             READY   STATUS    RESTARTS   AGE   IP               NODE                     NOMINATED NODE   READINESS GATES

kube-system   coredns-86c58d9df4-4cll4                         1/1     Running   0          31m   xxx.xxx.xxx.xxx       xxxxxxx.mylabserver.com   <none>           <none>

...

# 2. Containers, Orchestration, and Clusters

## 2.1 Orchestration

#### Pods

All containers can communicate with all other containers without NAT. All nodes can communicate with all containers (and vive versa) without NAT. The IP that a container sees itself as is the IP that others see it as.

Example:
-------
Pod IP Address: 10.20.0.3

Applications in a pod have access to shared volumes. Containers in the same pod can communicate on localhost.
This container is running a program that is exposed on port 80. Inside the pods: 127.0.0.1:80 Outside the pod 10.20.0.3:80
Other container is running a program that is exposed on port 2522 inside te pod 127.0.0.1:2522 and outside the pod 10.20.0.3:2522

kubectl get namespace

NAME          STATUS   AGE
default       Active   47h
kube-public   Active   47h
kube-system   Active   47h

###### Create new namespace

kubectl create namespace podexample
 
namespace/podexample created

kubectl get namespace
NAME          STATUS   AGE
default       Active   47h
kube-public   Active   47h
kube-system   Active   47h
podexample    Active   12s

##### Create pod-example. yaml

vim ./pod-example.yaml

apiVersion: v1
kind: Pod
metadata:
  name: examplepod
  namespace: podexample
spec:
  volumes:
  - name: html
    emptyDir: {}
  containers:
  - name: webcontainer
    image: nginx
    volumeMounts:
    - name: html
      mountPath: /usr/share/nginx/html
  - name: filecontainer
    image: debian
    volumeMounts:
    - name: html
      mountPath: /html
    command: ["/bin/sh", "-c"]
    args:
       - while true; do
           data >> /html/index.html;
           sleep 1;
         done

		 
create -f ./pod-example.yaml
pod/examplepod created


kubectl --namespace=podexample get pods
NAME         READY   STATUS    RESTARTS   AGE
examplepod   2/2     Running   0          2m29s

kubectl --namespace=podexample get pods -o wide
NAME         READY   STATUS    RESTARTS   AGE     IP           NODE                     NOMINATED NODE   READINESS GATES
examplepod   2/2     Running   0          2m49s   xx.xxx.x.x   xxxxxxx.mylabserver.com   <none>           <none>


### Networking

Network plugin

Networking and IP address management is provided by a network plugin. This can be a CNI plugin or a Kubenet plugin. 
Implementation varies according to the network plugin that is used. However, all K8s networking must follow these
three rules:
- All containers can communicate with all other containers without NAT
- All nodes can communicate with all containers (and vice versa) without NAT
- The IP address that the container sees itself as is the IP that others see it as

##### DNS 

Once there is a source for IP addresses in the cluster, DNS can start

##### Etcd

Etcd is updated with the IP information

##### Flannel 

The network plugin configures IPTables on the nodes to set up routing that allows communication between pods and nodes as well as 
with pods on other nodes within the cluster

##### DNS

All services that are defined in the cluster get a DNS record. This is the for the DNS service as well. Pods search DNS 
relative to their own namespace.

The DNS server schedules a DNS pod on the cluster and configures the kubelets to set the containers to use the cluster's DNS service

PodSpec DNS policies determine the way that a container uses DNS. Options include Default, ClusterFirst or None.


##### Service

kube-proxy this service definition selector matches pods with the "web" label. It exposes port 80 and targets port 9376 on the pods.

## Deployments

Name: nginx-deployments

Namespace: default

Creation Timestamp: Fri 15 Feb 2019 18:34:25

Labels: app=nginx

Annotations: deployment.kubernates.io/revision=2

…

A deployment is a defined object in the Kubernetes cluster
Deployments can be used to:
-	Roll out replica sets
-	Declare a new state for the pods in a replica set
-	Roll back to an earlier deployment
-	Scaling up deployments for load
-	Clean up replica sets that are no longer needed

…

Selector: app=nginx

Replicas: 3 desired| 3 updated| 3 total|3 available|0 unavailable

…

Deployments manage replicas sets
Deployments can be used to gradually remove and replace the pods defined in a replica set that is managed by the deployment
When an update is required, a new replica set is created and pods are brought up and down by the deployment controller

…

StrategyType: RollingUpdate

MinReadySeconds: 0

RollingUpdateStrategy: 25% max unavailable, 25% max surge

…

Deployments define an update strategy and allow for rolling updates
In this case, the pods will be replaced in increments of 25 % of the total number of pods

…

Pod Template:

Labels: app=nginx

Containers: 

Nginx:

Image: nginx:1.9.1

Port: 80/TCP

Environment: <none>

Mounts: <none>

Volumes: <none>

…

Deployments contain a PodSpec.
The PodSpec can be updated to increment the container version or the structure of the pod that are deployed. If the PodSpec is updated and differs from the current spec, this triggers the rollout process.



